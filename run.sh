#!/bin/bash
cd /
virtualenv -p /usr/bin/python2.7 coreproxy
cd coreproxy
source bin/activate
git clone https://github.com/seandsanders/coreproxy
git clone https://github.com/bravecollective/api
(cd api; python setup.py develop)
(cd coreproxy; python setup.py develop)
(cd coreproxy/brave/coreproxy; cp /config.py config.py)
(cd coreproxy/brave/coreproxy; python coreproxy.py)

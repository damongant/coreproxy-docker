#!/bin/bash
cd /
virtualenv -p /usr/bin/python2.7 ecdsa_keygen
cd ecdsa_keygen
source bin/activate
git clone https://github.com/seandsanders/ecdsa_keygen
(cd ecdsa_keygen; python setup.py develop)
python ecdsa_keygen/ecdsa_keygen.py
FROM ubuntu:14.04
MAINTAINER Alexander Schittler <me@damongant.de>

RUN apt-get update && apt-get install -y \
  python2.7 \
  python2.7-dev \
  python-virtualenv \
  git

ADD run.sh /run.sh
ADD config.py /config.py

RUN chmod +x /run.sh

CMD /run.sh
